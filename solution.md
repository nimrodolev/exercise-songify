# Songify Challenge Submission

## General Description

I wrote the exercise as a .net-core 2.2 C# server, as this is the programming language I'm most proficient with. The exercise contains a simple `DummyInMemoryDB` class that is used to mock the DB if the project is run, and it's populated on startup with some test data.

Below, as requested, are descriptions of the data scheme I chose and a list of issues and possible improvements.

### Deliverables

1. All of the annotated functions can be found in the [ListsController.cs](./src/Songify.Host/Controllers/ListsController.cs) file.
2. The scheme description is in the next part of this `readme` file.

## Data Scheme

I modeled the data into 2 entities - `Song` and `SongList`. This scheme was meant to make it easy to compile lists that referenced different songs. If needed, this scheme can be expanded and taken further apart - for example, by creating an `Artist` model and replacing the list of artist names contained in the `Song` model with a list of ID references to `Artist` entries. However, fot the simplicity of this exercise this degree of separation seemed enough.

Here's are simple tables to describe the scheme (corresponding C# model classes can be found [here](./src/Songify.Host/Models/)) - 

### Song

| Name                  | Type          | Constraints |
|-----------------------|---------------|-------------|
| ID                    | Integer       | Unique      |
| Name                  | String        |             |
| Artists               | Array<String> |             |
| OriginalArtists       | Array<String> |             |
| PlaybackLengthSeconds | Integer       |             |

### SongList

| Name                     | Type           | Constraints |
|--------------------------|----------------|-------------|
| ID                       | Integer        | Unique      |
| Name                     | String         |             |
| SongIds                  | Array<Integer> |             |
| TotalPlaybackTimeSeconds | Integer        |             |

## Known Issues & Possible Improvements
* Because of the constraints of the exercise (mainly the `IDB` interface), non of the server functions are run asynchronously. A more realistic .net application would leverage the [*Task Parallel Library* ](https://docs.microsoft.com/en-us/dotnet/standard/parallel-programming/task-parallel-library-tpl) to prevent executing threads from blocking when performing DB operations (which involve IO and can be optimized that way).
* The code performing the serialization `HashTable` to and from typed objects is very basic and is not production grade. Generally, this is not something one would do, and is again necessitated by the limitations of the `IDB` interface.
* Querying the DB for multiple items is done by performing multiple `getByID` operations (which is not ideal performance-wise), because this will still be more preferment in most cases than the only other alternative, which is calling `getAll` and filtering in memory. A change in the `IDB` interface to allow getting multiple items by ID in a single query could solve this.
* Because I implemented the server operations over any arbitrary list (accessed by it's ID), the naming of the functions isn't actually a good representation of what they do. I kept it so it's clear what requirement from the instructions corresponded to what function.
* No consideration has been made to concurrency, and the simple DB access function dictated performing unsafe updated over DB data. This can be solved using conditional updates if the DB interface was changed to support those.
* Some of the server code can be simplified and shortened using the asp.net framework. These changes were avoided to better comply with the exercise (for example, the `BuildResult` function is completely redundant).
* Ideally, each entity type (i.e. `Song`, `SongList`, etc.) should have a separate controller. For the purpose of brevity I chose to keep all functions grouped under one controller, which is why the `Lists` controller exposes both a */list/* endpoint and a */song/* endpoint.
* I used constants to configure table names and SongList limits, this should obviously be replaced with a configuration setting.

> This is probably not a complete list, but I feel it covers most of the major issues.