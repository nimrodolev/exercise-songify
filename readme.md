> These are the exercise's instructions, you can find my submission letter [here](./solution.md)
### Songify Coding Exercise

This task is to design and code a simple API and database schema for a very basic music service. The service will manage a list of favorite
songs on a music service that you are building, lets call it "songify". Plan to spend between 30 and 120 minutes on this, we are looking for a solid
proof of concept rather than production-level or runnable code.

Provide your solution to your recruiter via email in a compressed file. Please do not post or share the exercise, or commit to a public repository.
Once the exercise has been reviewed we will schedule the in-person interview. During the interview we will discuss your solution and potentially
expand upon the problem during the in-person portion of the interview.

### API Requirements

1. Add a song to the favorites list
2. Remove a song from the list
3. Retrieve a comma delimited list of all song names in the list
    1. The interface should allow the consumer to optionally pass a match string
    2. If a match string is passed, then only songs containing that string in their names should be returned
4. Bonus: Retrieve a comma delimited list of artists associated with a specific song, the API interface and behavior specifics are up to you

### Constraints

1. The song list can be no longer than 20 songs or 80 minutes
2. The code can be written in any common programming language that you are comfortable with

### Deliverables
1. A set of annotated functions in the language of your choice, organized as you see fit
2. A rough database schema description that supports the code.

### Code Background

Assume that you can map URLs to functions with a simple annotation and that you have the set of utility functions below, defined as appropriate
for your choice of language. Note that there is much more that would be done for a production system, feel free to use TODO comments to
indicate features and functionality that you would add if you were to take the code to production quality.

The functions below are the only access points to your database, feel free to convert these interfaces to the language of your choice.

```Hashtable DB.getByID(String tableName, Int rowID);```
\
Returns a hashtable of field name/value pairs in the specified row
Returns null if no song found for the ID

```Array DB.getAll(String tableName);```
\
Returns an array of hashtables representing all the data in the table, each row is as above in getByID

```boolean DB.insert(String tableName, Hashtable rowToInsert);```
\
Given a Hashtable containing all the fields of your table (except for rowID, which will be automatic), inserts a new row with the
given data.
Returns true if successful, false otherwise

```boolean DB.delete(String tableName, int rowID); // Deletes the row specified by row ID```
\
Returns true if successful, false otherwise
Note, the database interfaces are intentionally over simplified to provide the opportunity for you to demonstrate basic data manipulation
techniques
Search commands are omitted
Data structures are limited to simple variables and arrays

**Function results should be prepared using this function:**

```BuildResult(String content, Int httpStatus); // Returns a result object that the system will transform into an HTTP response```


For example, the simplest API function to get a song name could be as follows:

```javascript
@GET /song/name/{ID} // Maps the endpoint to this function with one required argument

function Result getSongName(Int ID) {
    resultArray = DB.getByID("Song", ID);
    name = resultArray[1];
    return BuildResult(name, 200);
}
```

### Database Background

Your solution should include a description of a rough logical schema that your code will access. The design should identify the primary entities,
keys, and relationships for songs, artists, and albums. You don't need to fill in every possible field that could be used, but the schema does need
to at least contain the tables and fields referenced by your code. Note that there may be both primary and secondary artists on a song. Feel free
to email with questions.

For example, a song table could be briefly defined as follows.

\*Song\*

ID - Integer, Unique
\
Name - Text

Feel free to use a more formal description of the schema if you prefer.