using System;

namespace Songify.Models
{
    public class SongList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int[] SongIds { get; set; }
        public int TotalPlaybackTimeSeconds { get; set; }
    }
}
