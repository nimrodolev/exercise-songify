﻿using System;

namespace Songify.Models
{
    public class Song
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string[] Artists { get; set; }
        public string[] OriginalArtists	 { get; set; }
        public int PlaybackLengthSeconds { get; set; }
    }
}
