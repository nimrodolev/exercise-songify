﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Songify.Abstractions;
using Songify.Models;

namespace Songify.Host
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var dummyDB = CreateDummyData();
            services.AddSingleton<IDB>(dummyDB);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }

        private IDB CreateDummyData()
        {
            var rand = new Random();
            var ser = new SimpleSerializer();
            var db = new DummyInMemoryDB();
            var songs = new Dictionary<int, Song>();
            for (int i = 0; i < 100; i++)
            {
                var artists = new List<string>();
                var originalArtists = new List<string>() { $"Original artist {i}" };
                for (int j = 0; j < (i % 4) + 1; j++)
                    artists.Add($"Artist {i}/{j}");
 
                var song = new Song
                {
                    ID = i + 1,
                    Artists = artists.ToArray(),
                    OriginalArtists = originalArtists.ToArray(),
                    Name = $"Hot song #{i}",
                    PlaybackLengthSeconds =  rand.Next(150, 310)
                };
                songs[song.ID] = song;
                db.insert("songs", ser.ObjectToDictionary(song));
            }

            for (int i = 0; i < 5; i++)
            {
                var list = new SongList
                {
                    ID = i + 1,
                    Name = $"List #{i}",
                };
                var ids = new HashSet<int>();
                for (int j = 0; j < 18; j++)
                {
                    while (true)
                    {
                        var r = rand.Next(1, 101);
                        if (!ids.Contains(r))
                        {
                            ids.Add(r);
                            list.TotalPlaybackTimeSeconds += songs[r].PlaybackLengthSeconds;
                            break;
                        }
                    }
                }
                list.SongIds = ids.ToArray();
                db.insert("lists", ser.ObjectToDictionary(list));
            }
            return db;
        }
    }
}
