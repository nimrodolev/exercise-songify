using System;
using System.Collections.Generic;
using System.Linq;
using Songify.Abstractions;

namespace Songify.Host
{
    // I'm just a dummy test DB, I'm not thread-safe or anything fancy
    public class DummyInMemoryDB : IDB
    {
        private const string ID_KEY_NAME = "ID";
        private Dictionary<string, Dictionary<int, IDictionary<string, object>>> _store = new Dictionary<string, Dictionary<int, IDictionary<string, object>>>();

        public bool delete(string tableName, int rowID)
        {
            var table = GetTable(tableName);
            if (!table.ContainsKey(rowID))
                return false;
            table.Remove(rowID);
            return true;
        }
        public IDictionary<string, object>[] getAll(string tableName)
        {
            var table = GetTable(tableName);
            return table.Values.ToArray();
        }
        public IDictionary<string, object> getByID(string tableName, int rowID)
        {
            var table = GetTable(tableName);
            IDictionary<string, object> res = null;
            if (!table.TryGetValue(rowID, out res))
                return null;
            return res;
        }
        public bool insert(string tableName, IDictionary<string, object> rowToInsert)
        {
            if (!rowToInsert.ContainsKey(ID_KEY_NAME))
                throw new Exception($"No '{ID_KEY_NAME}' key was found in {nameof(rowToInsert)}");
            if (!(rowToInsert[ID_KEY_NAME] is int))
                throw new Exception($"{ID_KEY_NAME} is not an Int32 instance");
            int id = (int)rowToInsert[ID_KEY_NAME];
            return insert(tableName, id, rowToInsert);

        }
        public bool insert(string tableName, int rowID, IDictionary<string, object> rowToInsert)
        {
            var table = GetTable(tableName);
            table[rowID] = rowToInsert;
            return true;
        }

        private Dictionary<int, IDictionary<string, Object>> GetTable(string tableName)
        {
            if (!_store.ContainsKey(tableName))
                _store[tableName] = new Dictionary<int, IDictionary<string, object>>();
            return _store[tableName];
        }
    }
}