using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Songify.Host
{
    internal class SimpleSerializer
    {
        public T DictionaryToObject<T>(IDictionary<string, object> source) where T : class, new()
        {
            var type = typeof(T);
            var obj = (T)Activator.CreateInstance(typeof(T));

            foreach (var item in source)
            {
                type.GetProperty(item.Key)
                    .SetValue(obj, item.Value, null);
            }

            return obj;
        }

        public IDictionary<string, object> ObjectToDictionary(object source)
        {
            return source.GetType()
                         .GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
                         .ToDictionary(pinfo => pinfo.Name, pinfo => pinfo.GetValue(source, null));
        }
    }
}