using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Songify.Abstractions;
using Songify.Models;

namespace Songify.Host
{
    public class ListsController : Controller
    {
        private const string SONGS_TABLE_NAME = "songs";
        private const string LISTS_TABLE_NAME = "lists";
        private const int MAX_AMOUNT_OF_SONGS = 20;
        private const int MAX_TOTAL_PLAYBACK_TIME_SECONDS = 60 * 80;

        private IDB _database;
        private SimpleSerializer _serializer = new SimpleSerializer();
        public ListsController(IDB database)
        {
            _database = database;
        }


        [HttpPost("/list/{listID}/{songID}")]
        public ActionResult AddSongToFavorites([FromRoute] int listID, [FromRoute]int songID)
        {
            //make sure the list exists
            var ldic = _database.getByID(LISTS_TABLE_NAME, listID);
            if (ldic == null)
                return BuildResult($"No song list with ID {listID} was found", 404);
            var list = _serializer.DictionaryToObject<SongList>(ldic);
            if (list.SongIds.Length >= MAX_AMOUNT_OF_SONGS)
                return BuildResult($"Song list with ID {listID} is already at it's maximum song capacity", 409);
            if (list.SongIds.Contains(songID))
                return BuildResult($"Song list with ID {listID} already contains song with ID {songID}", 409);

            //make sure the song exists
            var sdic = _database.getByID(SONGS_TABLE_NAME, songID);
            if (sdic == null)
                return BuildResult($"No song with ID {songID} was found", 404);
            var song = _serializer.DictionaryToObject<Song>(sdic);
            if (list.TotalPlaybackTimeSeconds + song.PlaybackLengthSeconds > MAX_TOTAL_PLAYBACK_TIME_SECONDS)
                return BuildResult($"Adding song {songID} to list {listID} will cause it to pass the maximum playback length capacity", 409);

            // update list
            list.SongIds = list.SongIds.Concat(new[] { song.ID }).ToArray();
            list.TotalPlaybackTimeSeconds += song.PlaybackLengthSeconds;
            if (!_database.insert(LISTS_TABLE_NAME, list.ID, _serializer.ObjectToDictionary(list)))
                BuildResult($"Failed to update list with ID {listID} in database", 500);
            return BuildResult(string.Empty, 200);
        }

        [HttpDelete("/list/{listID}/{songID}")]
        public ActionResult RemoveSongFromFavorites([FromRoute] int listID, [FromRoute]int songID)
        {
            //make sure the list exists
            var ldic = _database.getByID(LISTS_TABLE_NAME, listID);
            if (ldic == null)
                return BuildResult($"No song list with ID {listID} was found", 404);
            var list = _serializer.DictionaryToObject<SongList>(ldic);
            
            if (!list.SongIds.Contains(songID))
                return BuildResult($"List with ID {listID} does not contain a song with ID {songID}", 404);

            //make sure the song exists
            var sdic = _database.getByID(SONGS_TABLE_NAME, songID);
            if (sdic == null)
                return BuildResult($"No song with ID {songID} was found", 404);
            var song = _serializer.DictionaryToObject<Song>(sdic);

            // update list
            list.SongIds = list.SongIds.Where(i => i != song.ID).ToArray();
            list.TotalPlaybackTimeSeconds -= song.PlaybackLengthSeconds;
            if (!_database.insert(LISTS_TABLE_NAME, list.ID, _serializer.ObjectToDictionary(list)))
                return BuildResult($"Failed to update list with ID {listID} in database", 500);
            return BuildResult(string.Empty, 200);
        }

        [HttpGet("/list/{listID}/names")]
        public ActionResult<string> GetAllSongNamesFromFavorites([FromRoute] int listID, [FromQuery] string pattern = null)
        {
            //make sure the list exists
            var ldic = _database.getByID(LISTS_TABLE_NAME, listID);
            if (ldic == null)
                return BuildResult($"No song list with ID {listID} was found", 404);
            var list = _serializer.DictionaryToObject<SongList>(ldic);
            var songs = GetSongsByIDs(list.SongIds);
            if (songs.Count() != list.SongIds.Length)
            {
                var missing = list.SongIds.Except(songs.Select(s => s.ID));
                var msg = $"Not all songs for list with ID {listID} were found. Missing song IDs - {string.Join(",", missing)}";
                return BuildResult(msg, 404);
            }

            //filter list using pattern, if needed
            if (!string.IsNullOrEmpty(pattern))
                songs = songs.Where(s => s.Name.Contains(pattern));
            // project, join, and return
            return BuildResult(string.Join(",", songs.Select(s => s.Name)), 200);
        }

        [HttpGet("/song/{songID}/artists")]
        public ActionResult<string> GetAllArtistsForSong([FromRoute] int songID)
        {
            //make sure the song exists
            var sdic = _database.getByID(SONGS_TABLE_NAME, songID);
            if (sdic == null)
                return BuildResult($"No song with ID {songID} was found", 404);
            var song = _serializer.DictionaryToObject<Song>(sdic);
            
            var allRelatedArtists = song.Artists.Concat(song.OriginalArtists);
            return BuildResult(string.Join(",", allRelatedArtists), 200);
        }

        private IEnumerable<Song> GetSongsByIDs(int[] songIDs)
        {
            List<Song> songs = new List<Song>(songIDs.Count());
            foreach (var id in songIDs)
            {
                var sdic = _database.getByID(SONGS_TABLE_NAME, id);
                songs.Add(_serializer.DictionaryToObject<Song>(sdic));
            }
            return songs;
        }
    
        private ActionResult BuildResult(string content, int httpStatus)
        {
            return StatusCode(httpStatus, content);
        }
    }
}