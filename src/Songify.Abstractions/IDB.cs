﻿using System;
using System.Collections.Generic;

namespace Songify.Abstractions
{
    public interface IDB
    {
        IDictionary<string, object> getByID(string tableName, int rowID);
        IDictionary<string, object>[] getAll(string tableName);
        bool insert(string tableName, IDictionary<string, object> rowToInsert);
        bool insert(string tableName, int rowID, IDictionary<string, object> rowToInsert);
        bool delete(string tableName, int rowID);
    }
}
